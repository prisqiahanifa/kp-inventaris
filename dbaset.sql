-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Jul 2020 pada 08.23
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbaset`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `jenis_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `hardware_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(255) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_pembelian` datetime NOT NULL,
  `jangka_waktu` datetime NOT NULL,
  `harga` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id`, `user_id`, `jenis_id`, `status_id`, `cabang_id`, `hardware_type`, `jumlah`, `serial_number`, `tanggal_pembelian`, `jangka_waktu`, `harga`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 1, 1, 'Aser', 1000, '1001-6591-0051', '2020-06-23 00:00:00', '2025-06-23 00:00:00', '100000.00', '2020-06-23 02:58:03', '2020-06-23 02:58:03'),
(4, 1, 2, 1, 1, 'Kursi Kantor', 1022, '1111-2222-3333', '2020-05-19 00:00:00', '2025-05-19 00:00:00', '2000000.00', '2020-06-23 03:05:19', '2020-06-23 03:05:19'),
(5, 1, 1, 1, 3, 'Asus', 1000, '1001-6591-0033', '2020-07-02 00:00:00', '2025-07-02 00:00:00', '5000000.00', '2020-07-02 08:38:07', '2020-07-02 08:38:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cabang`
--

CREATE TABLE `cabang` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `cabang`
--

INSERT INTO `cabang` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Enterprise System Engineering', NULL, '2020-06-21 06:55:59'),
(2, 'PFT Labz', NULL, '2020-06-21 06:56:19'),
(3, 'Gartek Lab', '2020-06-23 03:05:31', '2020-06-23 03:05:31'),
(5, 'tekmi lab', '2020-06-24 05:12:02', '2020-06-24 05:12:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `investasi`
--

CREATE TABLE `investasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `status_investasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `investasi`
--

INSERT INTO `investasi` (`id`, `user_id`, `barang_id`, `amount`, `status_investasi`, `created_at`, `updated_at`) VALUES
(2, 1, 4, 22, 'Sudah Datang', '2020-07-09 09:09:42', '2020-07-09 09:10:22');

--
-- Trigger `investasi`
--
DELIMITER $$
CREATE TRIGGER `increase` AFTER UPDATE ON `investasi` FOR EACH ROW BEGIN
IF New.status_investasi != Old.status_investasi
THEN
    IF New.status_investasi = "Sudah Datang" 
    THEN
    UPDATE barang SET jumlah = jumlah+NEW.amount WHERE id=NEW.barang_id;
    ELSEIF NEW.status_investasi = "Waiting for payment" 
    THEN
    UPDATE barang SET jumlah = jumlah-NEW.amount
    WHERE id=NEW.barang_id;
    END IF;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Komputer', NULL, NULL),
(2, 'Furniture', NULL, NULL),
(3, 'ATK', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_05_14_095707_create_cabang_table', 1),
('2016_05_14_095722_create_jenis_table', 1),
('2016_05_14_095730_create_status_table', 1),
('2016_05_14_095741_create_barang_table', 1),
('2020_07_03_194004_create_peminjaman_table', 2),
('2020_07_05_201500_create_peminjaman_table', 3),
('2020_07_06_174631_create_peminjaman_table', 4),
('2020_07_09_160150_create_investasi_table', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `tanggal_peminjaman` datetime NOT NULL,
  `selesai_peminjaman` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `barang_id`, `user_id`, `cabang_id`, `amount`, `tanggal_peminjaman`, `selesai_peminjaman`, `update_time`, `create_time`) VALUES
(5, 5, 1, 1, 36, '2020-07-02 00:00:00', '2025-07-24 00:00:00', '2020-07-06 18:31:19', '2020-07-06 18:31:19');

--
-- Trigger `peminjaman`
--
DELIMITER $$
CREATE TRIGGER `dec` AFTER INSERT ON `peminjaman` FOR EACH ROW BEGIN
 UPDATE barang SET jumlah=jumlah-NEW.amount
 WHERE id=NEW.barang_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `status`
--

CREATE TABLE `status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `status`
--

INSERT INTO `status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Terawat', NULL, '2020-06-22 12:32:10'),
(2, 'Tidak Terawat', NULL, '2020-06-22 12:32:19'),
(4, 'Dalam Perbaikan', NULL, '2020-06-22 12:32:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no-foto.png',
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'operator',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `foto`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Laboran', 'admin@admin.com', '$2y$10$3UZiTefiR0l9mVJgMBNK2ummgaPFds32Ix5eW5GKCJVgVTaEBFfX6', 'TUTAIMG5ef0a72ac0da8nMVHK.jpg', 'admin', 'RRl07ibZh3t5u6nUPEWYoNcX8tVwaZBKLqhUms6ezd0bAxVNGoTQReyM8yLQ', NULL, '2020-07-01 04:17:54'),
(2, 'Ensyse', 'Ensyselab@gmail.com', '$2y$10$JH5l/fcXV6DUswR9aWtRbOIcAFbfKQ3gxTgjqxip9OlMv6kn5v4Pa', 'no-foto.png', 'operator', NULL, '2020-06-15 04:47:28', '2020-06-15 04:47:28'),
(4, 'Gartek ', 'gartek@gmail.com', '$2y$10$IKheIfsVmvImef0NB3XIf.OdwrlQOO6LB4iLH/IoxRrxybPOl/Nv.', 'no-foto.png', 'operator', NULL, '2020-06-24 05:29:58', '2020-06-24 05:29:58'),
(6, 'Sispromasi', 'sispromasi@gmail.com', '$2y$10$CvFTTQPERO2yEWS.giR8jeQOf.HOccZdmHhRhTw4nrSe.6xhzH1Zi', 'no-foto.png', 'operator', NULL, '2020-07-01 04:17:47', '2020-07-01 04:17:47');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_user_id_foreign` (`user_id`),
  ADD KEY `barang_jenis_id_foreign` (`jenis_id`),
  ADD KEY `barang_status_id_foreign` (`status_id`),
  ADD KEY `barang_cabang_id_foreign` (`cabang_id`);

--
-- Indeks untuk tabel `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `investasi`
--
ALTER TABLE `investasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `investasi_user_id_foreign` (`user_id`),
  ADD KEY `investasi_barang_id_foreign` (`barang_id`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `peminjaman_barang_id_foreign` (`barang_id`),
  ADD KEY `peminjaman_user_id_foreign` (`user_id`),
  ADD KEY `peminjaman_cabang_id_foreign` (`cabang_id`);

--
-- Indeks untuk tabel `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `investasi`
--
ALTER TABLE `investasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_cabang_id_foreign` FOREIGN KEY (`cabang_id`) REFERENCES `cabang` (`id`),
  ADD CONSTRAINT `barang_jenis_id_foreign` FOREIGN KEY (`jenis_id`) REFERENCES `jenis` (`id`),
  ADD CONSTRAINT `barang_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `barang_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `investasi`
--
ALTER TABLE `investasi`
  ADD CONSTRAINT `investasi_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`),
  ADD CONSTRAINT `investasi_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`),
  ADD CONSTRAINT `peminjaman_cabang_id_foreign` FOREIGN KEY (`cabang_id`) REFERENCES `cabang` (`id`),
  ADD CONSTRAINT `peminjaman_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
