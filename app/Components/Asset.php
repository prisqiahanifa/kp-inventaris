<?php

/**
 * Created by Irfan Mahfudz Guntur <irfanmg.com>
 * Asset Components
 */

namespace App\Components;

use App\Barang, App\User;
use App\Peminjaman;
use DB;

class Asset
{

  public function assetCount()
  {
    return Barang::count();
  }

  public function userCount()
  {
    return User::count();
  }
  public function peminjamanCount()
  {
    return Peminjaman::count();
  }
}
