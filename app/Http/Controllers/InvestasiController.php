<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Barang, App\Jenis;
use App\Cabang, App\Status;
use App\peminjaman, App\investasi;
use Auth, Redirect, Session;

class InvestasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inves = investasi::all();
        return view('dashboard.investasi.index', compact('inves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis = ['' => '--Pilih Jenis--'] + Jenis::pluck('name', 'id')->all();
        $cabang = ['' => '--Intansi--'] + Cabang::pluck('name', 'id')->all();
        $status = ['' => '--Pilih Status--'] + Status::pluck('name', 'id')->all();
        $barang = ['' => '--Pilih Barang--'] + Barang::pluck('hardware_type', 'id')->all();
        return view('dashboard.investasi.create', compact('jenis', 'cabang', 'status', 'barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inves = new investasi;
        $inves->barang_id = $request->barang_id;
        $inves->amount = $request->amount;
        $inves->user_id = Auth::user()->id;
        $inves->status_investasi = 'Dalam Proses . . .';
        $inves->save();
        Session::flash('message', 'penambahan investasi sukses!');
        return Redirect::to('dashboard/investasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inves = investasi::find($id);
        $jenis = ['' => '--Please Select--'] + Jenis::pluck('name', 'id')->all();
        $cabang = ['' => '--Pilih Cabang--'] + Cabang::pluck('name', 'id')->all();
        $status = ['' => '--Pilih Status--'] + Status::pluck('name', 'id')->all();
        $barang = ['' => '--Pilih Barang--'] + Barang::pluck('hardware_type', 'id')->all();
        return view('dashboard.investasi.edit', compact('barang', 'jenis', 'cabang', 'status', 'inves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inves = investasi::find($id);
        $inves->barang_id = $request->barang_id;

        $inves->status_investasi = $request->status_investasi;
        $inves->user_id = Auth::user()->id;

        $inves->save();
        Session::flash('message', 'Merubah investasi sukses!');
        return Redirect::to('dashboard/investasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inves = investasi::find($id);
        $inves->delete();
        Session::flash('message', 'Menghapus peminjaman Sukses!');
        return Redirect::to('dashboard/investasi');
    }
}
