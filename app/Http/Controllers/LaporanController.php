<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Barang;
use Spipu\Html2Pdf\Html2Pdf;

class LaporanController extends Controller
{
    public function index()
    {
		$asset = Barang::all();
      	return view('dashboard.laporan.index', compact('asset'));
	}

    public function pdf(Request $request)
    {		
		if($request->search === ""){
			$asset = Barang::all();
		} else {
			$asset = Barang::whereHas('cabang', function($query) {
						$query->where('name', 'like', '%' . \Request::input('search') . '%');
					})
					->orWhere('hardware_type', 'like', '%' . $request->search . '%')
					->orWhere('serial_number', 'like', '%' . $request->search . '%')
					->orWhereHas('jenis', function($query) {
						$query->where('name', 'like', '%' . \Request::input('search') . '%');
					})
					->orWhereHas('status', function($query) {
						$query->where('name', 'like', '%' . \Request::input('search') . '%');
					})
					->get();
		}

		$docpdf = new Html2Pdf('L', 'A4', 'en', true, 'UTF-8');
		$docpdf->pdf->SetTitle('Laporan Aset');
		$docpdf->writeHTML(view('dashboard.laporan.pdf', compact('asset')));
		$docpdf->output('laporan-aset.pdf');
		return view('dashboard.laporan.index', compact('asset'));
    	
    }
}
