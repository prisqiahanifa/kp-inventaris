<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Barang, App\Jenis;
use App\Cabang, App\Status;
use App\peminjaman;
use Auth, Redirect, Session;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjam = peminjaman::all();
        return view('dashboard.peminjaman.index', compact('pinjam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis = ['' => '--Pilih Jenis--'] + Jenis::pluck('name', 'id')->all();
        $cabang = ['' => '--Intansi--'] + Cabang::pluck('name', 'id')->all();
        $status = ['' => '--Pilih Status--'] + Status::pluck('name', 'id')->all();
        $barang = ['' => '--Pilih Barang--'] + Barang::pluck('hardware_type', 'id')->all();
        return view('dashboard.peminjaman.create', compact('jenis', 'cabang', 'status', 'barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pinjam = new peminjaman;
        $pinjam->barang_id = $request->barang_id;
        $pinjam->amount = $request->amount;
        $pinjam->cabang_id = $request->cabang_id;
        $pinjam->user_id = Auth::user()->id;
        $pinjam->tanggal_peminjaman = date("Y-m-d", strtotime($request->tanggal_peminjaman));
        $pinjam->selesai_peminjaman = date("Y-m-d", strtotime($request->selesai_peminjaman));
        $pinjam->save();
        Session::flash('message', 'peminjaman sukses!');
        return Redirect::to('dashboard/peminjaman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pinjam = peminjaman::find($id);
        $jenis = ['' => '--Please Select--'] + Jenis::pluck('name', 'id')->all();
        $cabang = ['' => '--Pilih Cabang--'] + Cabang::pluck('name', 'id')->all();
        $status = ['' => '--Pilih Status--'] + Status::pluck('name', 'id')->all();
        $barang = ['' => '--Pilih Barang--'] + Barang::pluck('hardware_type', 'id')->all();
        return view('dashboard.peminjaman.edit', compact('barang', 'jenis', 'cabang', 'status', 'pinjam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pinjam = peminjaman::find($id);
        $pinjam->barang_id = $request->barang_id;
        $pinjam->amount = $request->amount;
        $pinjam->cabang_id = $request->cabang_id;
        $pinjam->user_id = Auth::user()->id;
        $pinjam->tanggal_peminjaman = date("Y-m-d", strtotime($request->tanggal_peminjaman));
        $pinjam->selesai_peminjaman = date("Y-m-d", strtotime($request->selesai_peminjaman));
        $pinjam->save();
        Session::flash('message', 'Merubah pinjam sukses!');
        return Redirect::to('dashboard/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pinjam = peminjaman::find($id);
        $pinjam->delete();
        Session::flash('message', 'Menghapus peminjaman Sukses!');
        return Redirect::to('dashboard/peminjaman');
    }
}
