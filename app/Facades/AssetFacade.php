<?php
/**
 * Created by Irfan Mahfudz Guntur <irfanmg.com>
 * Aset Facade
 */
 namespace App\Facades;

 use Illuminate\Support\Facades\Facade;

 class AssetFacade extends Facade
 {

 	/**
 	* Get the registered name of the component.
 	*
 	* @return string
 	*/
 	protected static function getFacadeAccessor()
 	{
 		return 'asset';
 	}

 }
