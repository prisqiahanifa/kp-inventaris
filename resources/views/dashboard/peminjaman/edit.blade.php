@extends('layouts.app')

@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{!! asset('template/plugins/datepicker/datepicker3.css') !!}">
@endsection

@section('scripts')
<!-- bootstrap datepicker -->
<script src="{!! asset('template/plugins/datepicker/bootstrap-datepicker.js') !!}"></script>
<script>
    $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });
    });
</script>
@endsection

@section('content-header', 'Rubah peminjaman')

@section('breadcump')
<li>Dashboard</li>
<li>peminjaman</li>
<li class="active">Merubah Peminjaman</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Rubah Peminjaman</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            {!! Form::model($pinjam, array('route' => array('dashboard.peminjaman.update', $pinjam->id), 'method' => 'PUT')) !!}
            <div class="col-xs-6">
                <div class="form-group required {{ $errors->has('barang_id') ? ' has-error' : '' }}">
                    {!! Form::label('barang', old('barang'), '*', ['required' => 'required']) !!} <strong class="text-danger"> </strong>
                    <div class="form-group">

                        {!! Form::select('barang_id', $barang, old('barang_id'), array('class' => 'form-control', 'required' => 'required', 'id' => 'select-barang')) !!}
                    </div>
                </div>
                {{ Form::tutaText('amount', old('amount'), '*', ['required' => 'required']) }}


                <div class="form-group">
                    {!! Form::label('tanggal_peminjaman', 'Tanggal Peminjaman') !!}
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker" value="{{date('d-m-Y', strtotime($pinjam->tanggal_peminjaman))}}" name="tanggal_peminjaman" required />
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {!! Form::label('selesai_peminjaman', 'selesai peminjaman') !!}
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker1" value="{{date('d-m-Y', strtotime($pinjam->selesai_peminjaman  ))}}" name="selesai_peminjaman" required />
                    </div>
                    <!-- /.input group -->
                </div>

                <div class="form-group required {{ $errors->has('cabang_id') ? ' has-error' : '' }}">
                    {!! Form::label('cabang_id', 'Instansi') !!} <strong class="text-danger"> *</strong>
                    {!! Form::select('cabang_id', $cabang, old('cabang_id'), array('class' => 'form-control', 'required' => 'required')) !!}
                    @if ($errors->has('cabang_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cabang_id') }}</strong>
                    </span>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group pull-right">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a class="btn btn-small btn-warning" href="{{ URL::to('dashboard/peminjaman') }}">Cancel</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- /.box -->
    @endsection