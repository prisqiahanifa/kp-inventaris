@extends('layouts.app')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{!! asset('template/plugins/datepicker/datepicker3.css') !!}">
@endsection

@section('scripts')
<!-- bootstrap datepicker -->
<script src="{!! asset('template/plugins/datepicker/bootstrap-datepicker.js') !!}"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

    });
</script>
<script type="text/javascript" src="/mytuta/js/add.jenis.js"></script>
<script type="text/javascript" src="/mytuta/js/add.cabang.js"></script>
<script type="text/javascript" src="/mytuta/js/add.status.js"></script>
@endsection

@section('content-header', 'Peminjaman')

@section('breadcump')
<li>Dashboard</li>
<li>Asset</li>
<li class="active">Peminjaman Aset</li>
@endsection

@section('content')
<div id="code"></div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Peminjaman</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            {!! Form::open(array('route' => 'dashboard.peminjaman.store', 'method' => 'POST')) !!}
            <div class="col-xs-6">
                <div class="form-group required {{ $errors->has('barang_id') ? ' has-error' : '' }}">
                    {!! Form::label('barang', old('barang'), '*', ['required' => 'required']) !!} <strong class="text-danger"> </strong>
                    <div class="form-group">

                        <div class="input-group">
                            {!! Form::select('barang_id', $barang, old('barang_id'), array('class' => 'form-control', 'required' => 'required', 'id' => 'select-barang')) !!}
                            <span class="input-group-btn">
                                <button id="add-barang" type="button" class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                    {{ Form::tutaText('amount', old('amount'), '*', ['required' => 'required']) }}
                    <div class="form-group required {{ $errors->has('cabang_id') ? ' has-error' : '' }}">
                        {!! Form::label('cabang_id', 'Instansi') !!} <strong class="text-danger"> *</strong>
                        <div class="input-group">
                            {!! Form::select('cabang_id', $cabang, old('cabang_id'), array('class' => 'form-control', 'required' => 'required', 'id' => 'select-laboratorium')) !!}
                            <span class="input-group-btn">
                                <button id="add-cabang" type="button" class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                        @if ($errors->has('cabang_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cabang_id') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('tanggal_peminjaman', 'Tanggal Peminjaman') !!}
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="datepicker" name="tanggal_peminjaman" required />
                        </div>
                        <!-- /.input group -->
                        <div class="form-group">
                            {!! Form::label('selesai_peminjaman', 'selesai peminjaman') !!}
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker1" name="selesai_peminjaman" required />
                            </div>
                            <!-- /.input group -->
                        </div>



                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group pull-right">
                            {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                            <a class="btn btn-small btn-warning" href="{{ URL::to('dashboard/peminjaman') }}">Cancel</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>


            @include('dashboard.asset.modal._jenis')
            @include('dashboard.asset.modal._cabang')
            @include('dashboard.asset.modal._status')
            <!-- /.box -->
            @endsection