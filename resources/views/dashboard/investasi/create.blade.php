@extends('layouts.app')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{!! asset('template/plugins/datepicker/datepicker3.css') !!}">
@endsection

@section('scripts')
<!-- bootstrap datepicker -->
<script src="{!! asset('template/plugins/datepicker/bootstrap-datepicker.js') !!}"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });

    });
</script>
<script type="text/javascript" src="/mytuta/js/add.jenis.js"></script>
<script type="text/javascript" src="/mytuta/js/add.cabang.js"></script>
<script type="text/javascript" src="/mytuta/js/add.status.js"></script>
@endsection

@section('content-header', 'Investasi')

@section('breadcump')
<li>Dashboard</li>
<li>Asset</li>
<li class="active">Investasi Aset</li>
@endsection

@section('content')
<div id="code"></div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Investasi</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            {!! Form::open(array('route' => 'dashboard.investasi.store', 'method' => 'POST')) !!}
            <div class="col-xs-6">
                <div class="form-group required {{ $errors->has('barang_id') ? ' has-error' : '' }}">
                    {!! Form::label('barang', old('barang'), '*', ['required' => 'required']) !!} <strong class="text-danger"> </strong>
                    <div class="form-group">

                        <div class="input-group">
                            {!! Form::select('barang_id', $barang, old('barang_id'), array('class' => 'form-control', 'required' => 'required', 'id' => 'select-barang')) !!}
                            <span class="input-group-btn">
                                <button id="add-barang" type="button" class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                    {{ Form::tutaText('amount', old('amount'), '*', ['required' => 'required']) }}



                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group pull-right">
                            {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                            <a class="btn btn-small btn-warning" href="{{ URL::to('dashboard/investasi') }}">Cancel</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>


            @include('dashboard.asset.modal._jenis')
            @include('dashboard.asset.modal._cabang')
            @include('dashboard.asset.modal._status')
            <!-- /.box -->
            @endsection