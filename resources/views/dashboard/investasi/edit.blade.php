@extends('layouts.app')

@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{!! asset('template/plugins/datepicker/datepicker3.css') !!}">
@endsection

@section('scripts')
<!-- bootstrap datepicker -->
<script src="{!! asset('template/plugins/datepicker/bootstrap-datepicker.js') !!}"></script>
<script>
    $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker1').datepicker({
            autoclose: true
        });
    });
</script>
@endsection

@section('content-header', 'Edit investasi')

@section('breadcump')
<li>Dashboard</li>
<li>Edit Investasi</li>
<li class="active">Edit Investasi</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Investasi</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            {!! Form::model($inves, array('route' => array('dashboard.investasi.update', $inves->id), 'method' => 'PUT')) !!}
            <div class="col-xs-6">
                <div class="form-group required {{ $errors->has('barang_id') ? ' has-error' : '' }}">
                    {!! Form::label('barang', old('barang'), '*', ['required' => 'required']) !!} <strong class="text-danger"> </strong>
                    <div class="form-group">

                        {!! Form::select('barang_id', $barang, old('barang_id'), array('class' => 'form-control', 'required' => 'required', 'id' => 'select-barang')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group mx-4">
                        <?php
                        $A = null;
                        $B = null;

                        switch ($inves['status_investasi']) {
                            case 'Dalam Proses . . .':
                                $A = 'checked';
                                break;
                            case 'Sudah Datang':
                                $B = 'checked';
                                break;
                        } ?>

                        <input type="radio" name="status_investasi" value="Dalam Proses . . ." <?php echo $A; ?>>
                        <label class="mr-2">Dalam Proses</label>

                        <input type="radio" name="status_investasi" value="Sudah Datang" <?php echo $B; ?>>
                        <label class="mr-2">Sudah Datang</label>

                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a class="btn btn-small btn-warning" href="{{ URL::to('dashboard/investasi') }}">Cancel</a>
                    {!! Form::close() !!}
                </div>


                <!-- /.box-body -->
            </div>

            <!-- /.box -->
            @endsection